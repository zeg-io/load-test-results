# Load Test Results

This CLI was created not to replace other CLIs which were written to load test, but to provide a hybrid which is capable of sending thousands of requests, but then analyze the results based on expected response values.

## Config

test-package.json
```json
{
  "title": "Bulk Test Sasquach.com's API",
  "numberOfRequests": 1000,
  "concurrency": "infinite",
  "testResultsFile": "./test-results.html",
  "autoOpenTestResultsFile": true,
  "calls": [
    {
      "title": "Harry Henderson's Device Call",
      "headers": [
        { "Authorization": "Bearer jfklsu78usdinjkGUYH...JKG78tuhgvhfdsGJtYy^fgYJHghj" } 
      ],
      "protocol": "https",
      "url": "https://api.sasquach.com/v1/find/device/8812675646",
      "exectedResults": { ... },
      "exectedResultsFile": "./test-package-expected-results-harry.json"
    },
    { ... }
  ]
}
```

## Usage

```sh
load-test-results ./test-package.json
```