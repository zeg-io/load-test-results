const chalk = require('chalk')
const jobFileName = process.argv[process.argv.length - 1]
const selectedConfig = require(jobFileName)
const workJob = require('./lib/workJob')

if (!selectedConfig)
  return console.error(`Unable to open [${selectedConfig}]`)

let initializeMessage = `
Initializing job with config [${chalk.yellow(jobFileName)}]
Job:         ${chalk.blue(selectedConfig.title)}
Requests:    ${chalk.blue(selectedConfig.numberOfRequests)}
Concurrency: ${chalk.blue(selectedConfig.concurrency)}
Results:     ${chalk.blue(selectedConfig.testResultsFile)}

${chalk.bold('Calls:')}
`
selectedConfig.calls.forEach(call => {
  initializeMessage += ` - ${chalk.blue(call.title)}\n`
})

console.info(initializeMessage)

workJob(selectedConfig)
.then(callResultsArray => {

  callResultsArray.forEach(call => {
    console.info(`
  Title:     ${chalk.blue(call.title)}
  Successes: ${chalk.green(call.successCount)}
  Failures:  ${chalk.yellow(call.failures.length)}
  Errors:    ${chalk.red(call.errors.length)}
  `)
  })
  console.error('---- DONE ----')
})




