const path = require('path'),
      chalk = require('chalk'),
      timestamp = require('./timestamp'),
      axios = require('axios')
const sha256 = require('js-sha256')
const validateExpectedResults = require('./validateExpectedResults')

const processCall = ({ call, index }) => new Promise(async (resolve, reject) => {
  const { title, headers, protocol, method, url, expectedResultsFile, expectedResults } = call
  const now = timestamp()
  let wasSuccess, wasError
  let status,
      returnData,
      result = { unexpected: 'content' }

  try {
    let expected = require(path.join('../', expectedResultsFile))

    if (!expected) expected = expectedResults
    if (!expected) throw Error(`No expected results found for ${title}!`)

    const expectedSha = sha256(JSON.stringify(expected))

    // DO THE CALL!
    let callResponse, wasError, wasSuccess
    try {
      callResponse = await axios.get(url, { headers, method })

      const { status, data } = callResponse

      if (status < 300 && validateExpectedResults(index, data, expectedSha))
        wasSuccess = true
      else
        wasSuccess = false
    } catch (err) {
      console.error(err)
      wasError = true
      result = err.message
    }

    if (wasSuccess) {
      returnData = { statusCode: 200, result: null }
      status = chalk.green('[200]')
    } else if (wasError) {
      returnData = { statusCode: 500, result: {} }
      status = chalk.red('[500]')
    } else {
      returnData = { statusCode: 200, result }
      status = chalk.yellow('[200]')
    }
    console.info(`${now} ${status} ${title}`)
    returnData.index = index
    resolve(returnData)
  } catch (err) {
    if (err.message.includes('Unexpected end of JSON'))
      console.error(`Malformed expected results file [${expectedResultsFile}]`)
    else
      console.error(err.message)
    reject('Error processing a call, terminating.')
  }
})

module.exports = processCall
