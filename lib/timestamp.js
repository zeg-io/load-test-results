const { format } = require('date-fns')

const timestamp = () => format(new Date(), 'YYYY-MM-DD/HH:mm:ss.SSS')

module.exports = timestamp
