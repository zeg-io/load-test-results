const sha256 = require('js-sha256'),
      fs = require('fs'),
      timestamp = require('./timestamp'),
      path = require('path')
const validateExpectedResults = (index, results, sha) => {
  const stringifiedResults = JSON.stringify(results)
  const doesItMatch = sha256(stringifiedResults) === sha

  if (!doesItMatch) {
    const logPath = path.join(process.cwd(), './ltr-failure-logs')

    if (!fs.existsSync(logPath))
      fs.mkdirSync(logPath)
    fs.writeFileSync(path.join(logPath, index + '] ' + timestamp().replace('/', '--') + '.json'), stringifiedResults, { flag: 'wx' })
  }

  return doesItMatch
}

module.exports = validateExpectedResults
