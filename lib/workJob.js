const processCall = require('./processCall')
const clone = require('clone'),
      fs = require('fs-extra')
      path = require('path'),
      pMap = require('p-map')

const workJob = config => new Promise(async (resolve, reject) => {
  const { numberOfRequests, concurrency, testResultsFile, autoOpenTestResultsFile, calls } = config

  const callModel = {
    title: '',
    successCount: 0,
    failures: [],
    errors: []
  }
  let requestCursor,
      jobQueue = []

  const logPath = path.join(process.cwd(), './ltr-failure-logs')
  let callPromiseArray = []

  fs.emptyDirSync(logPath)

  let callResultsSummaryArray = calls.map(call => {
    let newModel = clone(callModel)

    newModel.title = call.title
    return newModel
  })

  // Up to the numberOfRequests
  for (requestCursor = 0; requestCursor < numberOfRequests; requestCursor++) {
    // console.info('job', requestCursor)
    // Each call in the job

    let callIdx,
        callCount = calls.length

    for (callIdx = 0; callIdx < callCount; callIdx++) {
      jobQueue.push({ call: calls[callIdx], index: callIdx })
    }
  }

  const callResultsArray = await pMap(jobQueue, processCall, { concurrency })

  callResultsArray.forEach(callResult => {
    // const callResult = await processCall(calls[callIdx])
    const { result, index } = callResult

    // console.info('call', idx)
    if (result && Object.keys(result).length > 0) {
      // Unexpected results returned
      callResultsSummaryArray[index].failures.push(callResult)
    } else if (result && Object.keys(result).length === 0) {
      // There an actual error.
      callResultsSummaryArray[index].errors.push(callResult)
    } else {
      // No problem
      callResultsSummaryArray[index].successCount += 1
    }
  })

  resolve(callResultsSummaryArray)
})

module.exports = workJob
